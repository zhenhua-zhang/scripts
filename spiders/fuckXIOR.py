#!/usr/bin/python3
#-*- coding: utf-8 -*-
#########################################################
# File Name: fuckXIOR.py
# Author: Z.Z
# Email: zhzhang2015@sina.com
# Start Time: Wed 06 Jun 2018 06:14:43 PM CEST
#########################################################
#
import re
import sys
import json
import requests
from copy import deepcopy
from requests import Session

get_url = "https://xior-booking.com"
post_url = "https://xior-booking.com/wp-content/plugins/xior-spaces/live/search.php"
data_dic = {
    'Accept': '*/*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh;q=0.9',
    'Connection': 'keep-alive',
    'Content-Length': '291',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Cookie': 'PHPSESSID=cb13a4fb86d12a6a27c3a112af61f31c',
    'Host': 'xior-booking.com',
    'Origin': 'https',
    'Referer': 'https',
    'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Mobile Safari/537.36',
    'X-Requested-With': 'XMLHttpRequest',
#}

#dic_page_1 = {
    "action": "xior_spaces_ajax_search",
    "security": "62c1caf04f",
    "filter[lang]": "en",
    "filter[location_id]": "",
    "filter[city_id]": "",
    "filter[space_type_id]": "",
    "filter[price][]": "25",
    "filter[price][]": "1500",
    "filter[size][]": "10",
    "filter[size][]": "175",
    "filter[order]": "0",
    "filter[page]": "1",
}
#json_page_1 = json.dumps(dic_page_1)

#dic_page_2 = deepcopy(dic_page_1)
#dic_page_2["filter[page]"] = "2"
# json_page_2 = json.dumps(dic_page_2)

sess = Session()

sess.get(get_url)

response_page_1 = sess.post(post_url, data=data_dic)
response_page_1_text = response_page_1.text
accom_page_1 = re.findall("(Oosterhamrikkade .*?)\<", response_page_1_text)


data_dic["filter[page]"] = "2"
response_page_2 = sess.post(post_url, data=data_dic)
response_page_2_text = response_page_2.text
accom_page_2 = re.findall("(Oosterhamrikkade .*?)\<", response_page_2_text)

all_accom = accom_page_1 + accom_page_2

argv = sys.argv
if len(argv) == 1:
    for x in sorted(all_accom):
        print(x)
else:
    for x in sorted(all_accom):
        if "105-1" in x or "105-3" in x or "105-5" in x:
            print(x)
