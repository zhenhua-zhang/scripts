#!/usr/bin/python3
# -*- coding: utf-8 -*-

#########################################################
# File Name: crawlScholarGoogle.py
# Author: Z.Z
# Email: zhzhang2015@sina.com
# Start Time: Thu 23 Aug 2018 04:07:52 PM CEST
#########################################################
#

import os
import copy

from urllib import parse
from requests import Session
from bs4 import BeautifulSoup


class ScholarGoogle:

    def __init__(self, keywords, startYear=None, endYear=None):

        self.S = Session()
        self.keywords = keywords
        self.startYear = startYear
        self.endYear = endYear
        self.query = self.configRequenst()
        self.targetUrl = self.configUrl(self.query)

    def configRequenst(self, **options):
        """
        Args:
        Returns:
        """
        basicQuery = {'q': self.keywords}

        if self.startYear:
            basicQuery['as_ylo'] = str(self.startYear)

        if self.endYear:
            basicQuery['as_yhi'] = str(self.endYear)

        _query = copy.deepcopy(basicQuery)

        keys = options.keys()
        if 'keywords' in keys:
            _ = options.pop('keywords')
            _query['q'] = options['keywords']

        if 'startYear' in keys:
            _ = options.pop('startYear')
            _query['startYear'] = options['startYear']

        if 'endYear' in keys:
            _ = options.pop('endYear')
            _query['endYear'] = options['endYear']

        for _key, _value in options.items():
            _query[_key] = _value

        return parse.urlencode(_query)

    def configUrl(self, query, **options):
        scheme, netloc = 'https', 'scholar.google.nl'
        path, fragment = '/scholar', ''
        keys = options.keys()
        if 'scheme' in keys:
            scheme = options['scheme']
        if 'netloc' in keys:
            netloc = options['netloc']
        if 'path' in keys:
            path = options['path']
        if 'fragment' in keys:
            fragment = options['fragment']

        return parse.urlunsplit((scheme, netloc, path, query, fragment))

    def requestByGET(self, url=None):
        if url:
            url = url
        else:
            url = self.targetUrl
        responesByGET = self.S.get(url)
        text = responesByGET.text
        responesByGET.close()

        return text

    def requestsByXHR(self, getUrl=None, postUrl=None):
        sess = Session()
        if getUrl:
            responseGET = sess.get(getUrl)
        else:
            return None

        if postUrl:
            responsePost = sess.post(postUrl)
        else:
            return None

        text = responsePost.text
        return text

    def textParser(self, _text=None):
        if not _text:
            return None

        soup = BeautifulSoup(_text, 'lxml')
        return soup

    def summary(self):
        print(self.keywords)


class Text:
    """ Class Text: processing text from response
    """
    def __init__(self, text):
        self.text = text


tmp = ScholarGoogle('ran seq analysis for the diagnosis of musular dystrophy',
                    startYear='2018', endYear=1990)
print(tmp.query)
print(tmp.configUrl(tmp.query))

# SplitResult(scheme='https', netloc='scholar.google.nl', path='/scholar',
# query='q=rna+seq+analysis+for+the+diagnosis+of+muscular+dystrophy&hl=en&'+
# 'as_sdt=0&as_vis=1&oi=scholart', fragment='')
# sess = Session()
# response = sess.get(url)
