import scrapy


class GoogleScholar(scrapy.Spider):
    ''' Main class googleScholar
    '''

    name = 'google_scholar'
    start_urls = ['https://scholar.google.nl']
    keywords = ['abc']

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        for a in response.xpath('//a/@href').extract():
            yield {'a': a}

