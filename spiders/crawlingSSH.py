#########################################################
# File Name: crawlingSSH.py
# Author: Z.Z
# Email: zhzhang2015@sina.com
# Start Time: Thu 04 Jan 2018 05:36:37 PM CET
#########################################################
#
#!/usr/bin/python3
#-*- coding: utf-8 -*-
# 说来也奇怪，以前也试过这样，但是不成功，不知道今天咋就成功了，太神奇了。估计得等明天在试试，万一不行了呢，毕竟我的浏览器本地还有很多Cookies，虽然不知道是不是这个原因，还是等等看吧。
import json
import logging 
from requests import Session

true = True
responseResultFile = "response.json"

logging.basicConfig(level = logging.INFO)
logger = logging.getLogger("REQUESTS")

baseUrl = "https://aanbod.sshxl.nl/ons-huuraanbod"
xhrUrl = 'https://aanbod.sshxl.nl/usercontrols/kim/aanbod/ikwilhuren.asmx/GetAanbodData'

logger.info(" Start to set up session with "+baseUrl)
s = Session()
s.get(baseUrl)

logger.info(" Setting up payload and request headers")
payload = json.dumps({"aanbodtypes":[],"filters":[],"filtersoorten":["Aggregatie","HuurtoeslagMogelijk","Type"],"skipFirstFilter":true})
requestHeader = {'Accept':'application/json, text/javascript, */*; q=0.01',
                 'Accept-Encoding':'gzip, deflate, br',
                 'Accept-Language':'zh-CN,zh;q=0.8',
                 'Connection':'keep-alive',
                 'Content-Length':'',
                 'Content-Type':'application/json; charset=UTF-8',
                 #'Cookie':'BataviaWonen.Session=o1tm5k2dbkpgjphfdkcgqlkd; Language=en-US; __atuvc=18%7C52%2C27%7C1; __atuvs=5a4e471d4b1514da000; _ga=GA1.2.695652008.1514296068; _gid=GA1.2.2144178308.1514815281; _gat=1; __AntiForgeryToken=xskxx0owd5Ls6/SCkOiy5KZ5GU3uNxxnITprVs0EQWJE6+FLmEvq+OxT7W7teUPFnM2yhVNAhz8QkPEH2WEA7B8N56jjjL6ekkMz7SrtdF4=; huuraanbodsettings-%2Fons-huuraanbod=%7B%22filters%22%3A%5B%5D%2C%22aanbodtypes%22%3A%5B%5D%2C%22sorting%22%3A%7B%22field%22%3A%22Naam%22%2C%22reverse%22%3Afalse%2C%22istext%22%3Atrue%7D%2C%22template%22%3A%22templateLijst%22%7D',
                 'Host':'aanbod.sshxl.nl',
                 'Origin':'https://aanbod.sshxl.nl',
                 'Referer':'https://aanbod.sshxl.nl/',
                 'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
                 'X-Requested-With':'XMLHttpRequest'
            }
s.headers.update(requestHeader)

logger.info(" Post data to xhr dealer.")
r = s.post(xhrUrl, data = payload)
try:
    logger.info(" Fetch data from the xhr dealer")
    responseText = r.text
except:
    logger.error(" Failed at POST.")
    raise

logger.info(" Dump data to file: " + responseResultFile)
data = json.loads(responseText)
with open(responseResultFile, 'w') as f:
    f.writelines(json.dumps(data, indent=4))

