# Crawl on www.nastol.com.ua
import re
import os
import time
import requests
from requests import Session

base_url = "https://www.nastol.com.ua/"
downloaded_url_pool = []

for x in range(401, 1000):
    job_url = base_url + "devushki/page/" + str(x)
    thumb_ses = Session()
    thumb_res = thumb_ses.get(job_url)
    thumb_con = thumb_res.text

    detail_url_pool = []
    rex = "\"(https://www.nastol.com.ua/devushki/.*\.html)\""
    detail_url_pool.extend(re.findall(rex, thumb_con))
    thumb_ses.close()

    for each_url in detail_url_pool:
        each_url_ses = Session()
        each_url_res = each_url_ses.get(each_url)
        each_url_con = each_url_res.text
        each_url_ses.close()
        pic_rex = "href=\"/(images/.*\.jpg)\""
        pic_url_pool = re.findall(pic_rex, each_url_con)

        if(len(pic_url_pool)) == 0:
            continue

        pic_url = base_url + pic_url_pool[0]
        if pic_url in downloaded_url_pool:
            continue

        pic_name = os.path.split(pic_url)[-1]
        pic_ses = Session()
        pic_res = pic_ses.get(pic_url, stream=True)


        with open(pic_name, "wb") as f:
            f.write(pic_res.content)

        print("Pic: "+pic_name + " was downloaded.")

        pic_ses.close()

    time.sleep(2)

