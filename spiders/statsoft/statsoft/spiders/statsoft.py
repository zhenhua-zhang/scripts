import scrapy
from scrapy import Selector

class StatSoft(scrapy.Spider):
    name = 'statsoft'
    domain = 'http://www.statsoft.com'
    start_urls = ['http://www.statsoft.com/Textbook']

    def start_requests(self):
        url = self.start_urls[0]
        yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        aList = response.xpath('//a[contains(@href, "extboo")]').re('.*img.*')
        for a in aList:
            a_selector = Selector(text=a, type="html")
            sub_url = a_selector.xpath('//a/@href').extract()[0]
            sub_url = self.domain + sub_url
            text = a_selector.xpath('//a/text()').extract()[0]
            text = "_".join( [x for x in text.split(" ") if x and x != '/'])
            yield response.follow(sub_url, callback=self.parse_sub_url)

    def parse_sub_url(self, response):
        content_xpath = '//*[@id="dnn_ContentPane2"]'
        content_pane = response.xpath(content_xpath).extract_first()
        content_pane_selector = Selector(text=content_pane, type='html')
        test_xpath_list = [
            '//p/text()', '//h2/text()', '//h3/text()', '//h4/text()', 
            '//a/text()', '//strong/text()',  '//a/@href', '//img/@src' 
            '//i/text()', '//a/@href/i/text()'
        ]
        tests_xpath =  ' | '.join(test_xpath_list)
        texts = content_pane_selector.xpath(tests_xpath).extract()
        yield {"text": texts}
