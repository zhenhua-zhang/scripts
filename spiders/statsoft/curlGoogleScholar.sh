URL="https://scholar.google.com/scholar?q=Deep+learning"

curl -H "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8" \
    -H "accept-encoding: gzip, deflate, br" \
    -H "accept-language: zh-CN,zh;q=0.9,en;q=0.8" \
    -H "cache-control: max-age=0" \
    -H "cookie: CONSENT=WP.271476; 1P_JAR=2018-09-11-17; GSP=LM=1536690371:S=j6STR1xoQx3yAkHe; NID=138=Xdi5Y_EUGFCrSwwad_JkAZV2VUs4YhM_VPzii8hC8dpjjXxWF8sKg6X5Tk_l93pfAu8NMxT4ACgHgmFx38BrnasA8DUZPQ8VsCSfflT1-9pZMUOiMpCvDOqSPUfQsLE5" \
    -H "upgrade-insecure-requests: 1" \
    -H "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36" \
    ${URL}
