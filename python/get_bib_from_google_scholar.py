#!/usr/bin/env python3
import argparse
from urllib import parse

import requests
from bs4 import BeautifulSoup

def parser_args():
    parser = argparse.ArgumentParser(
        prog = "fbib",
        description="A script to fetch BibTex format citation from Google Scholar"
    )

    subparsers = parser.add_subparsers()

    config_parser = subparsers.add_parser("config", help="Configure a BibTex database.")
    config_parser.add_argument()

    init_parser = subparsers.add_parser("init", help="Create a new BibTex database")
    init_parser.add_argument()

    fetch_parser = subparsers.add_parser("fetch")
    fetch_parser.add_argument()

    merge_parser = subparsers.add_parser("merge")
    merge_parser.add_argumeng()

    delete_parser = subparsers.add_parser("delete", help="Delete given entry")
    delete_parser.add_argument()

    return parser


class BibTexFetcher:
    HEADERS = {
        "Accept": "*/*",
        "Accept-Encoding": "gzip,deflate,br",
        "Accept-Language": "en-US,en;q=0.5",
        "Connection": "keep-alive",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0",
        "X-Requested-With": "XHR"
    }

    def __init__(self):
        """"""

    def encode_url(self, path, query, *extra_path):
        path += "/".join(extra_path)
        return "{}?{}".format(path, query)

    def encode_query(self, query, language="en", **kwargs):
        query_dict = {"q": query, "hl": language}
        query_dict.update(kwargs)
        return parse.urlencode(query_dict)

    def fetch(self, article_name):
        """"""
        query_str = self.encode_query(article_name)
        url = self.encode_url("https://scholar.google.com/scholar", query_str)

        sess = requests.Session()
        response = sess.request(method="get", url=url, headers=self.HEADERS)
        body = response.text
        soup = BeautifulSoup(body, features="lxml")

        article_items = soup.find_all("div", attrs={"class": "gs_r gs_or gs_scl"})
        for item in article_items:
            item_attrs = item.attrs
            data_cid = item_attrs["data-cid"]
            data_rp = item_attrs["data-rp"]

            title_link = item.find("a", attrs={"id": data_cid})
            _article_title = title_link.text # The title of current item
            _article_link = title_link.get("href")  # The link of current item

            item_query = "info:{}:scholar.google.com/".format(data_cid)
            item_query_str = self.encode_query(query=item_query, language="en", output="cite", scirp=data_rp)
            item_url = self.encode_url("https://scholar.google.com/scholar", item_query_str)

            item_response = sess.request("get", url=item_url,
                                         headers=self.HEADERS)
            item_body = item_response.text
            item_soup = BeautifulSoup(item_body, features="lxml")

            cite_divs = item_soup.find_all("a", attrs={"class": "gs_citi"})
            cite_dict = {}
            for cite_item in cite_divs:
                cite_type = cite_item.text
                cite_url = cite_item.get("href")
                cite_response = sess.request("get", url=cite_url,
                                             headers=self.HEADERS)
                cite_dict[cite_type] = cite_response.text


class BibTexDatabase:
    def __init__(self):
        """"""

class Downloader:
    def __init__(self):
        """"""
