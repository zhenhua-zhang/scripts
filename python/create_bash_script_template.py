#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""A Python script to create a template of bash script.
"""

import os
import argparse

import numpy as np
import pandas as pd

parser = argparse.ArgumentParser()

bash_template = """#!/bin/bash
#
## An script to create bash script template
#

#!/bin/bash
#
# Description: {description:}
# Email      : {email:}
# Author     : {author:}
# License    : {license:}
# Create date: {date:}
# Last update: {date:}
#

ERRO() {
    echo -e "[E]: $@" >&2 && exit 1
}

WARN() {
    echo -e "[W]: $@" >&2
}

INFO() {
    echo -e "[I]: $@"
}

echo_help() {
    cat <<EOF

Usage:
    bash $(basename $0) [options]

Help:
  -w|--workdir WORKDIR
    The path script will work in.
  -i|--input INPUT-FILE
    The path to the input-file.
  -o|--output OUTPUT-FILE
    The path to the output-file.
  -h|--help
    Print this help context and exit.

More information please contact {author:} <{email:}>

EOF
    exit 0
}


opt=$(getopt -l "input:,output::,help" -- "i:o::huvV" $@)
eval set -- ${opt}
while 1; do
    case $1 in
        -i|--input)
            shift && input=$1 ;;
        -o|--output)
            shift && output=$1 ;;
        -w|--workdir)
            shift && workdir=$1 ;;
        -h|--help)
            echo_help ;;
        --)
            shift && break;;
    esac
    shift
done

input=${input:?-i/--input is required!}
output=${output:?-o/--output is required!}
workdir=${workdir:?-w/--workdir is required!}
"""

print(bash_template)
