#!/bin/bash
opt=$(getopt -l "" -- "" $@)
eval set -- ${opt}

while 1; do
    case $1 in
        --help) echo_help ;;
        --) shift && break ;;
    esac
    shift
done
