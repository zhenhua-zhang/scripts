# Some useful snippets

ERRO() {
	echo -e "[ERRO]: $1" >&2 && exit -1
}

WARN() {
	echo -e "[WARN]: $1" >&2
}

INFO() {
	echo -e "[INFO]: $1"
}

check_dir() {
	[ -d $1 -a $1 ] && INFO "Found directory ${1}" || ERRO "NOT found directory ${1}"
}

check_file() {
	[ -f $1 -a $1 ] && INFO "Found file ${1}" || ERRO "NOT found file ${1}"
}

is_exist() {
	[ -f $1 ] && ERRO "Found file ${1}" || INFO "${1} is not existed ..."
}

check_success() {
	cmd=${2:-Last command}
	[ $1 -eq 0 ] && INFO "${cmd} success!" || ERRO "${cmd} non-zero exit ..."
}

# Join your path by each directory name
jpth() {
	echo ${@} | tr -s '/' ' ' | tr -s ' ' '/'
}

ckmkdir() {  # Check the directory, or create it if not exists.
	${1:?Argument is empty, please specify dir path}
	[ -d ${1} ] && INFO "Find directory ${1}" \
		|| ( [ -w  $(dirname $1) ] && mkdir -p $1 || ERRO "NOT writable")
}

echo_version() {
	cat << EOF

$(basename $0), Version ${SCRIPT_VERSOIN:=UNKNOWN}
EOF
}

echo_usage() {
	cat <<EOF

Usage: ./$0 -i/--input INPUT-PATH -o/--output OUTPUT-PATH [options]
EOF
}

echo_help() {
	echo_version
	echo_usage
	cat <<EOF

Help:
  -i, --input    Required. Action: store_value
    The path to the input-file.
  -o, --output    Optional. Action: store_value
    The path to the output-file.
  -v, --Verbose    Optional. Action: store_true
    Verbose while running the script.
  -h, --help    Optional. Action: print_info
    Print this help context and exit.
  -u, --usage    Optional. Action: print_info
    Print usage context and exit.
  -V, --version    Optional. Action: store_true
    Print version of current script and exit.

More information please contact Zhenhua Zhang <zhenhua.zhang217@gmail.com>
EOF
	exit 0
}


# { HEAD:: template for handling command line options
opt=$(getopt -l "input:,output::,help,usage,verbose,version" -- "i:o::huvV" $@)
eval set -- ${opt}
while true; do
	case $1 in
		-i|--input)
			shift && tgsv=$1 ;;
		-o|--output)
			shift && jmsv=$1 ;;
		-v|--verbose)
			verbose=1 ;;
		-h|--help) 
			echo_help && exit 0 ;;
		-u|--usage) 
			echo_usage && exit 0 ;;
		-V|--version)
			echo_version && exit 0 ;;
		--)
			shift && break;;
	esac
	shift
done
#   TAIL:: template for handling command line options } 
